# Using Podman with WSL2
This Guide was created for Ubuntu 20.04 running in WSL2
## Podman Installation
1. Install Binaries
    ```bash
    # Add the Kubic repository
    sudo sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
    # Add their key to the keyring
    wget -nv https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/xUbuntu_${VERSION_ID}/Release.key -O- | sudo apt-key add -
    # Install Podman with additional dependencies + Python3 (needed for docker-compose)
    sudo apt update && sudo apt install podman buildah cri-o-runc python3 python3-pip
    # Symlink the runc binary from Kubic to /usr/bin (the one from the official Ubuntu repo is too old)
    sudo ln -s /usr/lib/cri-o-runc/sbin/runc /usr/sbin/runc
    ```
    After running the commands, everything is installed. Now we need to configure things, to make it Work in WSL.

2. Open your `.bashrc` file (or the rc file of your shell) and add the following (taken from [here](https://dev.to/bowmanjd/using-podman-on-windows-subsystem-for-linux-wsl-58ji)):
    ```bash
    if [[ -z "$XDG_RUNTIME_DIR" ]]; then
      export XDG_RUNTIME_DIR=/run/user/$UID
      if [[ ! -d "$XDG_RUNTIME_DIR" ]]; then
        export XDG_RUNTIME_DIR=/tmp/$USER-runtime
        if [[ ! -d "$XDG_RUNTIME_DIR" ]]; then
          mkdir -m 0700 "$XDG_RUNTIME_DIR"
        fi
      fi
    fi
    ```

3. Configure Container registries to use:
    ```bash
    echo -e "[registries.search]\nregistries = ['docker.io', 'quay.io']" | sudo tee /etc/containers/registries.conf
    ```

4. Configure Podman to not rely on Systemd  
Copy podman confg file `sudo cp /usr/share/containers/containers.conf /etc/containers/`
then edit file `/etc/containers/containers.conf` by adding/midfing lines under the `[engine]` section like below:
    ```ini
    #...
    [engine]
    #...
    cgroup_manager = "cgroupfs"
    events_logger = "file"
    #...
    ```

5. You should now be able to run containers rootless. Try it out:
    ```bash
    $ podman run --rm hello-world

    Resolved "hello-world" as an alias (/etc/containers/registries.conf.d/shortnames.conf)
    Trying to pull docker.io/library/hello-world:latest...
    Getting image source signatures
    Copying blob 0e03bdcc26d7 done
    Copying config bf756fb1ae done
    Writing manifest to image destination
    Storing signatures

    Hello from Docker!
    This message shows that your installation appears to be working correctly.
    #...
    ```
    
Note: In case of error

    ```bash
    ERRO[0000] error loading cached network config: network "podman" not found in CNI cache
    WARN[0000] falling back to loading from existing plugins on disk
    ERRO[0000] Error tearing down partially created network namespace for container 
    ```

configure distor to use legacy iptables

    ```bash
	sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
	sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
    ```

Source
https://github.com/microsoft/WSL/issues/7948#issuecomment-1043467915


If you don't need docker-compose, you can stop now.

## Getting docker-compose running
This currently does not work rootless (see https://www.redhat.com/sysadmin/podman-docker-compose).
1. Install docker-compose for root:
    ```bash
    sudo -H pip3 install docker-compose
    ```
2. Copy the file `docker` from this repo to `/usr/bin`. This will redirect calls to the docker binary to Podman. This file is taken from https://archlinux.org/packages/community/x86_64/podman-docker/.  
*Note: Make sure, the file can be executed. Run `chmod +x docker` prior copying.*

3. Copy the file `podman-service` (or  `podman-tcp-service`) to `/etc/init.d`. Start the Podman system socket by running:
    ```bash
    sudo service podman-service start
    sudo service podman-service status

    sudo service podman-tcp-service start
    sudo service podman-tcp-service status
    ```
This workaround is necessary, as we don't have Systemd in WSL.  
The script will start Podman with listening on a unix socket, which is compatible to the Docker API (available since Podman 2.0). It will also symlink the Podman socket to the location, the Docker socket normally resides.  
*Note: Make sure, the file can be executed. Run `chmod +x podman-service` prior copying.*

4. Now you can use docker-compose by running it with root permissions, i.e. `sudo docker-compose up`.
5. To stop the Podman system socket, run `sudo service podman-service stop`.

## WSL aouto start
To auto start when linux distro boots edit /etc/wsl.conf by adding (if file does not exists create it)

    ```bash
    [boot]
    command = service podman-tcp-service start
    ```

Source: https://docs.microsoft.com/en-us/windows/wsl/wsl-config


